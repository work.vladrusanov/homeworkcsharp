﻿using System;
using Logger;

namespace Validator {
    internal class ValidatorService {

        const string ERROR_MSG = "Not valid data";

        public static string getMessage(string data, string type) {
            return $"{data} - {type}";
        }

        public static void showData() {
            string data = Console.ReadLine();
            string type = Console.ReadLine();

            switch (type) {
                case "bool":
                    try {
                        Boolean.Parse(data);
                        string message = getMessage(data, type);
                        LoggerService.log(message);
                    } catch {
                        LoggerService.log(ERROR_MSG);
                    }
                    break;

                case "int":
                    try {
                        Int32.Parse(data);
                        string message = getMessage(data, type);
                        LoggerService.log(message);
                    } catch (FormatException) {
                        LoggerService.log(ERROR_MSG);
                    }
                    break;

                case "double":
                    try {
                        Double.Parse(data);
                        string message = getMessage(data, type);
                        LoggerService.log(message);
                    }
                    catch {
                        LoggerService.log(ERROR_MSG);
                    }
                    break;

                    // will be the same for other cases

                default:
                    LoggerService.log(ERROR_MSG);
                    break;
            }
            

        } 

    }
}
